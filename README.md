# Creative Project Index

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A partial list of personal creative projects and project ideas, including writing ones, I'm keeping temporarily until I can merge it into [my 'Creative Sandbox'](https://gitlab.com/RandomDSdevel/creative-sandbox).  

## Projects

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Projects I currently have in my work queue include:  

### Infrastructure

 - [My 'Semantic Looseleaf Workspace' format](https://gitlab.com/semantic-looseleaf-workspace):  

   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Generate a workspace that lets you organize your creative endeavors using YAML front matter.  

   - [Specification](https://gitlab.com/semantic-looseleaf-workspace/specification)
   - [Tooling](https://gitlab.com/semantic-looseleaf-workspace/tooling)
   - [Template Repository](https://gitlab.com/semantic-looseleaf-workspace/template-repository)

   _Status:_  **Stalled; On Development Hiatus**

### Writing Projects

 - _Crossroads_ (Working title:)  

   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A multi-part project consisting of:  

   - Some new stories for the shared _FreeRIDErs_ setting.  
   - One or more new stories for the the collaborative 'MORFS' ('**M**assive **O**ntogonetic **R**egulatory **F**ailure **S**yndrome') universe.  
   - A crossover between them canon to neither _FreeRIDErs_ nor the MORFS universe.  

   _Status:_  **Long Stalled; In Extended Development Purgatory**; Not Yet Public
 - An unnamed original sci-fi setting.  

   _Status:_  Not Yet Public
 - _Swords and Sourcery_ (Title tentative)

   _Status:_  Early/Nascent Pre-Alpha.  **Stalled; On Development Hiatus.**  
 - An as-yet-untitled _Sword Art Online_ OC-insert fusion multi-crossover idea.  

   _Status:_  Early/Nascent Pre-Alpha
